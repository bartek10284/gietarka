#include <LiquidCrystal.h>
#include <EEPROM.h>





//wyswietlacz zmienne
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5 

#define BUTTON 12

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

int lcd_key  = 0;
int adc_key_in  = 0;


//obsluga lcd klawiszy 

int read_LCD_buttons()
{
 adc_key_in = analogRead(0);      // read the value from the sensor 
 // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // we add approx 50 to those values and check to see if we are close
 if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
 // For V1.1 us this threshold
 if (adc_key_in < 50)   return btnRIGHT;  
 if (adc_key_in < 250)  return btnUP; 
 if (adc_key_in < 450)  return btnDOWN; 
 if (adc_key_in < 650)  return btnLEFT; 
 if (adc_key_in < 850)  return btnSELECT;  

 // For V1.0 comment the other threshold and use the one below:
/*
 if (adc_key_in < 50)   return btnRIGHT;  
 if (adc_key_in < 195)  return btnUP; 
 if (adc_key_in < 380)  return btnDOWN; 
 if (adc_key_in < 555)  return btnLEFT; 
 if (adc_key_in < 790)  return btnSELECT;   
*/

 return btnNONE;  // when all others fail, return this...
}

// ============= 

//switch wyswietlacza zmienne
int stanLCD = 1;
int timer = 0;
unsigned long pTime = 0;


////============

// zmienne dla switch counter
int stanE = 1;
int timer2 = 0;

#define B 13

 int counter = 0; 
 


//==++++

// zmienne informacyjne
int TimeN = 0;
int TimeS = 0;
int Q1 = 0;
int Q2 = 0;


// zmienne switch funkcyjny
int stan = 1;



//servo pid

int LOL = 0;
char reads =0;
int A_SIG = 1, B_SIG=0;
volatile int count;
int pwm_pin = 11;
int ina = A2;
int inb = A5;
int stan1 =1;

int dir;
float PID;
float K,I,D;

//________________
void setup() { 

  Serial.begin(9600);
  pinMode (B,OUTPUT);
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  timer = 3;
  int prvKey = btnNONE;
//_______sevo
  pinMode (pwm_pin,OUTPUT);
  pinMode (ina,OUTPUT);
  pinMode (inb,OUTPUT);
   pinMode (A4,OUTPUT);
    pinMode (A3,OUTPUT);
  pinMode (2,INPUT);
  pinMode (3,INPUT);
  digitalWrite(2,HIGH);
  digitalWrite(3,HIGH); 
  attachInterrupt(0,A_RISE,RISING);
  attachInterrupt(1,B_RISE,RISING);
  pinMode (BUTTON,INPUT);
  digitalWrite(BUTTON,HIGH);
  
  count=0;
  
  K=0.2;
//--------------servo  

  

}

void loop() {  

  //---PID
  int error = LOL-count;  
  if(error < 0) dir=-1; else dir = 1;
  
  PID = K*error;
  if (PID < 0 ) PID = PID*dir;
  if (PID > 255 ) PID = 255; 

  //___PID

  //servo loop
  if (count >= LOL){
     ccw((int)PID);
  }else if(count <= LOL){
     cw((int)PID);
  }else{  
    analogWrite(pwm_pin,30);  
    digitalWrite(ina,HIGH);
    digitalWrite(inb,HIGH);
    
  }
  
  
  //serwo loop

  
  lcd_key = read_LCD_buttons();
  
 // counter switch
  switch(lcd_key){
    case btnUP:{
      counter++;
      delay(100);
      break;
     }
      case btnDOWN:{
      counter--;
      delay(100);
      break;
      }  
    
    }

 



  
//switch wyswietlacza zmienne

 
 switch(stanLCD){
      
      case 1:{        
        lcd.setCursor(0,0);        
        lcd.print("    BEND-001     ");
        if(timer == 0){
         stanLCD = 2; 
         
        } 
         break;     
      }
      case 2:{
        stan = 1;
        lcd.setCursor(0,0);
        lcd.print("USTAW CZAS      ");
        lcd.setCursor(0,1);
        lcd.print("NAGRZEWANIA     ");
        lcd.setCursor(12,1);
        lcd.print(counter);
       if(lcd_key == btnSELECT){
          stanLCD = 3;
          TimeN = counter;
          counter = 0;        
          delay(300);    
        }
        
        break;
        }

       case 3:{
        lcd.setCursor(0,0);
        lcd.print("USTAW CZAS      ");
        lcd.setCursor(0,1);
        lcd.print("STUDZENIA       ");
        lcd.setCursor(12,1);
        lcd.print(counter);
        if(lcd_key == btnSELECT){
          TimeS = counter;
          counter = 0; 
          stanLCD = 4;          
          delay(300);   
        }
        
        break;
        }

       case 4:{
        lcd.setCursor(0,0);
        lcd.print("USTAW POZYCJE   ");
        lcd.setCursor(0,1);
        lcd.print("SERWA1          ");
        lcd.setCursor(12,1);
        lcd.print(counter);
        if(lcd_key == btnSELECT){
          Q1 =counter;
          counter = 0; 
          stanLCD = 5;           
          delay(300);        
        }       
        break;           
       }  

        case 5:{
        lcd.setCursor(0,0);
        lcd.print("GIECIE          ");
        lcd.setCursor(0,1);
        lcd.print("SELECT TO END          ");
        if(lcd_key == btnSELECT){
          stanLCD = 2; 
          delay(300);        
        }       
        break;           
       }
      
  
  
  
  
  
  }


  // switch funkcyjny 

  switch(stan){ //oczekiwanie na ziolony przycisk
    case 1:{
      LOL=0;
      digitalWrite(B,HIGH);     
      digitalWrite(A4,LOW);
      if(digitalRead(BUTTON)==LOW && stanLCD == 5) {           
            stan = 2;
            timer2 = TimeN;
      
      }
      break;
      }
     case 2:{//nagrzewanie
      LOL = 0; 
      digitalWrite(A4,LOW);      
      if(Q1 != 0)digitalWrite(B,LOW); else digitalWrite(B,HIGH); 
      if(timer2 == 0){               
        stan = 3;
        timer2 = TimeS; 
      }               
 
     break; 
     }  
     case 3:{//ustw pozycje serwa studzenie
      LOL = map(Q1,0,180,0,-4000);      
      digitalWrite(B,HIGH);
      digitalWrite(A4,LOW);
       if(timer2 == 0){              
        stan = 4;
        timer2 = 3;
      } 
      break;    
      }
      case 4:{  //powrot serwa i brzeczyk
       LOL=0;     
       digitalWrite(B,HIGH);
       digitalWrite(A4,HIGH);
       if(timer2 == 0){              
        stan = 1;
    
      }      break;}     
    
  }
  Serial.println(error);  

 delay(10);



////============

unsigned long cTime = millis();
if(cTime-pTime >= 500){
    pTime = cTime;
    timer--;
    if(timer < 0) timer = 0;
    timer2--;
    if(timer2 < 0) timer2 = 0;  
  }
 

}

void A_RISE(){
  detachInterrupt(0);
  A_SIG=1;
  if(B_SIG==0){ count++; }else if(B_SIG==1){ count--;} 
  attachInterrupt(0,A_FALL,FALLING);  
  }


void A_FALL(){
  detachInterrupt(0);
  A_SIG=0;
  if(B_SIG==1){ count++; }else if(B_SIG==0){ count--;} 
  attachInterrupt(0,A_RISE,RISING);  
  }


void B_RISE(){
  detachInterrupt(1);
  B_SIG=1;
   if(A_SIG==1){ count++; }else if(A_SIG==0){ count--;} 

  attachInterrupt(1,B_FALL,FALLING);  
  }
 
void B_FALL(){
  detachInterrupt(1);
  B_SIG=0;
  if(A_SIG==0){ count++; }else if(A_SIG==1){ count--;} 
 
  attachInterrupt(1,B_RISE,RISING);  
  }

  void cw(int pwm){
       analogWrite(pwm_pin,pwm);  
       digitalWrite(ina,LOW);
       digitalWrite(inb,HIGH);   
    
    }
 void ccw(int pwm){   
     analogWrite(pwm_pin,pwm);  
     digitalWrite(ina,HIGH);
     digitalWrite(inb,LOW);
 }






